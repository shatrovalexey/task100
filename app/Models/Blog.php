<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Base as ModelBase ;
use App\Models\Product as ModelProduct ;
use Illuminate\Validation\Rule as ValidationRule ;

class Blog extends ModelBase {
    use HasFactory ;

	protected $table = 'blog' ;

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer' ;

	/**
	* Дата-время
	*
	* @var bool $timestamps
	*/
	public $timestamps = false ;

    /**
	 * Заполняемые поля
	 *
     * @var array
     */
    public $fillable = [ 'href' , 'title' , 'body' , 'description' , 'product' , 'views' , 'time_create' , ] ;

	const DATE_FORMAT = 'Y-m-d\TH:i:s' ;

	/**
	* Правила валидации атрибутов
	*
	* @return array
	*/
	public static function rules( ) : array {
		return [
			'title' => [ 'required' , 'string' , ] ,
			'href' => [ 'required' , 'string' , ] ,
			'body' => [ 'required' , 'string' , ] ,
			'description' => [ 'required' , 'string' , ] ,
			'product' => [ 'required' , 'string' , ValidationRule::exists( app( ProductModel::class )->getTable( ) , 'name' ) ] ,
			'views' => [ 'required' , 'integer' , 'default:0' , ] ,
			'time_create' => [ 'required' , 'integer' , ] ,
		] ;
	}

	/**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function products( ) {
		return $this->belongsTo( 'App\Product' ) ;
	}

	/**
	* Строка ко времени UNIX
	*
	* @param string $str - строка с датой\временем
	*
	* @return integer - время UNIX
	*/
	public static function toTime( string $str ) : int {
		return strToTime( $str ) ;
	}

	/**
	* Время UNIX в строку
	*
	* @param integer $time - Время UNIX
	*
	* @return string - строка
	*/
	public static function fromTime( int $time ) : string {
		return gmDate( static::DATE_FORMAT , $time ) ;
	}
}