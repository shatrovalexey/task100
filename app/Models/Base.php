<?php

namespace App\Models;

use Illuminate\Http\Request ;
use Illuminate\Database\Eloquent\Model ;
use Illuminate\Database\Eloquent\Builder ;
use Validator ;

/**
* Базовая модель
*/
class Base extends Model {
	/**
	* @var array $rules - правила валидации полей
	*/
	public $rules = [ ] ;

	/**
	* Вывод ошибок валидации
	*
	* @return array - список ошибок
	*/
    public function errors( ) : array {
		$rules = static::get_rules( ) ;
		$data = $this->attributesToArray( ) ;
		$validator = Validator::make( $data , $rules ) ;

		if ( ! empty( static::$attributeNames ) ) {
			$validator->setAttributeNames( static::$attributeNames ) ;
		}
		if ( ! $validator->fails( ) ) {
			return [ ] ;
		}

		return $validator->errors( )->toArray( ) ;
	}

	/**
	* Список правил валидации полей
	*
	* @return array - список правил валидации
	*/
	public static function get_rules( ) : array {
		if ( $result = static::_rules( ) ) {
			return $result ;
		}
		if ( $result = static::rules( ) ) {
			return $result ;
		}
		if ( $result = static::$rules ) {
			return $result ;
		}

		return [ ] ;
	}

	/**
	* Список правил валидации полей из свойства класса
	*
	* @return array - список правил валидации
	*/
	public static function rules( ) : array {
		return static::$rules ;
	}

	/**
	* Список правил валидации полей из защищённого метода класса
	*
	* @return array - список правил валидации
	*/
	protected static function _rules( ) : array {
		return static::rules( ) ;
	}

	/**
	* Генерация фэйкового экземпляра класса
	*
	* @return array - экземпляр класса
	*/
	public static function generate( ) {
		$model = new static( ) ;

		return $model ;
	}

	/**
	* Генерация произвольного количества экземпляров класса
	*
	* @param int $min - минимальное количество
	* @param int $max - максимальное количество
	*
	* @return array - список экземпляров класса
	*/
	public static function generate_array( int $min = 0 , int $max = 0 ) {
		if ( empty( $max ) ) {
			$max = static::count( ) ;
		}

		$result = [ ] ;
		$count = rand( $min , $max ) ;

		while ( $count -- ) {
			$result[] = static::generate( ) ;
		}

		return $result ;
	}

	/**
	* Выборка произвольного количества экземпляров класса из БД
	*
	* @param int $min - минимальное количество
	* @param int $max - максимальное количество
	*
	* @return array - список экземпляров класса
	*/
	public static function generate_random( int $min = 0 , int $max = 0 ) {
		if ( empty( $max ) ) {
			$max = static::count( ) ;
		}

		$count = rand( $min , $max ) ;

		return static::inRandomOrder( )->limit( $count )->get( ) ;
	}

	/**
	* Выборка произвольного идентификатора
	*
	* @param bool $nullable - есть вероятность, что результат будет = null
	*
	* @return int - идентификатор
	*/
	public static function generate_id( bool $nullable = false ) {
		if ( $nullable && ( rand( 1 , 2 ) == 2 ) ) {
			return null ;
		}

		return static::inRandomOrder( )->value( 'id' ) ;
	}

	/**
	* Генерация e-mail
	*
	* @return string - e-mail
	*/
	public static function generate_email( ) {
		$range = 'abcdefghijklmnoprstuvwxyz.-' ;
		$range_max = mb_strlen( $range ) - 1 ;

		$gen_name = function( ) use( &$range , &$range_max ) {
			$name = '' ;
			$name_len = rand( 2 , 20 ) ;

			do {
				while ( mb_strlen( $name ) < $name_len ) {
					$name .= mb_substr( $range , rand( 2 , $range_max ) , 1 ) ;
				}

				$name = preg_replace( '{[\.\-]{2,}}' , '' , $name ) ;
				$name = preg_replace( '{^[\.\-]|[\.\-]$}' , '' , $name ) ;
			} while ( mb_strlen( $name ) < $name_len ) ;

			return $name ;
		} ;

		return sprintf( '%s@%s.%s' , $gen_name( ) , $gen_name( ) , [ 'ru' , 'com' , 'org' , ][ rand( 0 , 2 ) ] ) ;
	}

	/**
	* Генерация названия
	*
	* @param int $min_length - минимальная длина названия
	* @param int $max_length - максимальная длина названия
	*
	* @return string - название
	*/
	public static function generate_slug( int $min_length = 1 , int $max_length = 50 ) : string {
		return static::generate_name( false , '' , 'ABCDEFGHIJKLMNOPQRSTUVWXYZ' , 'ABCDEFGHIJKLMNOPQRSTUVWXYZ' , $min_length , $max_length ) ;
	}

	/**
	* Генерация имени или названия
	*
	* @param bool $nullable - есть вероятность, что результат будет = null
	* @param string $additional - дополнительные символы для генерации
	*
	* @return string - имя или название
	*/
	public static function generate_name(
		bool $nullable = false ,
		string $additional = '' ,
		string $range_low = 'абвгдеёжзийклмнопрстуфхцчшщъыьэюя- ' ,
		string $range_up = 'АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ' ,
		int $min_length = 1 ,
		int $max_length = 49
	) {
		if ( $nullable && ( rand( 1 , 2 ) == 2 ) ) {
			return null ;
		}

		$range_low .= $additional ;
		$range_low_max = mb_strlen( $range_low ) - 1 ;
		$range_up_max = mb_strlen( $range_up ) - 1 ;
		$result_len = rand( $min_length , $max_length ) ;
		$is_first_letter = true ;
		$result = '' ;

		do {
			while ( mb_strlen( $result ) < $result_len ) {
				if ( $is_first_letter ) {
					$result .= mb_substr( $range_up , rand( 0 , $range_up_max ) , 1 ) ;
				} else {
					$result .= mb_substr( $range_low , rand( 0 , $range_low_max ) , 1 ) ;
				}

				$is_first_letter = preg_match( '{\W$}us' , $result ) ;
			}

			$result = preg_replace( '{\W{2,}}us' , '' , $result ) ;
			$result = preg_replace( '{^\W+|\W+$}us' , '' , $result ) ;
		} while ( mb_strlen( $result ) < $result_len ) ;

		return $result ;
	}
}