<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Base as ModelBase ;

class Product extends ModelBase {
    use HasFactory ;

	protected $table = 'products' ;

    /**
     * The "type" of the ID
     * 
     * @var string
     */
    protected $keyType = 'string' ;

	/**
	* Дата-время
	*
	* @var bool $timestamps
	*/
	public $timestamps = false ;

    /**
	 * Заполняемые поля
	 *
     * @var array
     */
    public $fillable = [ 'name' , 'href' , ] ;

	/**
	* Правила валидации атрибутов
	*
	* @return array
	*/
	public static function rules( ) : array {
		return [
			'name' => [ 'required' , 'string' , 'unique:' . static::$table , ] ,
			'href' => [ 'required' , 'string' , ] ,
		] ;
	}
}
