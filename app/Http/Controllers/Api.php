<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request as Request ;
use App\Models\Blog as BlogModel ;
use App\Models\Product as ProductModel ;
use DB ;

/**
* @OA\Schema(@OA\Xml(name="Api"))
* @OA\Info(
*	title="Контроллер API",
*	version="1.0.0",
*	@OA\Contact(
*		email="mail@shatrov.ru"
*	)	
* )
*/
class Api extends BaseController {
	/**
	* Отключил CSRF
	*
	* @var bool $csrf
	*/
	protected $csrf = false ;

	/**
	* Возможные значения для размера страницы
	*
	* @const array
	*/
	const LIMIT_VALUES = [ 1 , 3 , 5 , 10 , 20 , ] ;

    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

	/**
	*	@OA\Post(
	*		path="/api/v1/blog/{id}" ,
	*		operationId="blog_update" ,
	*		tags={"blog"},
	*		description="Обновление сущности Blog",
	*		@OA\Parameter(
	*			name="id" , example="10", description="Идентификатор сущности" , in="path" ,
	*				@OA\Schema( type="integer" )
	*		) ,
	*		@OA\RequestBody(
	*			required=true,
	*			@OA\JsonContent(
	*				required={"title", "href", "body", "description", "product", "views", "time_create"},
	*				@OA\Property(property="title", type="string", description="Заголовок"),
	*				@OA\Property(property="href", type="string", description="Ссылка"),
	*				@OA\Property(property="body", type="string", description="Тело"),
	*				@OA\Property(property="description", type="string", description="Описание"),
	*				@OA\Property(property="product", type="string", description="Продукт"),
	*				@OA\Property(property="views", type="integer", description="Просмотры"),
	*				@OA\Property(property="time_create", type="integer", description="Дата-время создания")
	*			),
	*		),
	*		@OA\Response(
	*			response=200,
	*			description="Успешная операция" ,
	*			@OA\JsonContent(
	*				@OA\Property(property="result", type="bool", description="Результат")
	*			)
	*		),
	*		@OA\Response(
	*			response=404,
	*			description="Не найдено",
	*		)
	*	)
	*/
	/**
	* Обновление сущности Blog
	* @method POST
	*
	* @return bool
	*/
	public function blog_update( int $id , Request $request ) {
		$blog = BlogModel::find( $id ) ;

		if ( empty( $blog ) ) {
			abort( 404 ) ;
		}

		return [ 'result' => $blog->update( $request->all( ) ) , ] ;
	}

	/**
	*	@OA\Get(
	*		path="/api/v1/blogs" ,
	*		operationId="blogs" ,
	*		tags={"blog"},
	*		description="Список блогов",
	*		@OA\RequestBody(
	*			required=true,
	*			@OA\JsonContent(
	*				@OA\Property(property="limit", type="integer", description="Размер страницы"),
	*				@OA\Property(property="page", type="integer", description="Страница"),
	*				@OA\Property(property="time_create", type="integer", description="Дата-время создания"),
	*				@OA\Property(property="views", type="integer", description="Просмотры"),
	*				@OA\Property(property="product", type="string", description="Продукт")
	*			),
	*		),
	*		@OA\Response(
	*			response=200,
	*			description="Успешная операция" ,
	*			@OA\JsonContent(
	*				@OA\Property(property="current_page", type="integer", description="Текущая страница"),
	*				@OA\Property(property="next_page_url", type="string", description="URL следующей страницы"),
	*				@OA\Property(property="path", type="string", description="Текущий URL"),
	*				@OA\Property(property="per_page", type="integer", description="Размер страницы"),
	*				@OA\Property(property="prev_page_url", type="integer", description="URL предыдущей страницы"),
	*				@OA\Property(property="to", type="integer", description="Последний идентификатор на странице"),
	*				@OA\Property(property="total", type="integer", description="Общее количество объектов"),
	*				@OA\Property(property="links", type="array", description="Список ссылок на страницы",
	*					@OA\Items(
	*						type="array",
	*						collectionFormat="multi",
	*						@OA\Items(
	*							@OA\Property( property="url", type="string", description="URL страницы" ),
	*							@OA\Property( property="label", type="string", description="Ярлык ссылки" ),
	*							@OA\Property( property="active", type="boolean", description="Текущая ли страница" )
	*						)
	*					)
	*				),
	*				@OA\Property(property="data", type="array", description="Список объектов Blog",
	*					@OA\Items(
	*						type="array",
	*						collectionFormat="multi",
	*						@OA\Items(
	*							@OA\Property( property="id", type="integer", description="Идентификатор" ),
	*							@OA\Property( property="title", type="string", description="Заголовок" ),
	*							@OA\Property( property="views", type="integer", description="Просмотры" ),
	*							@OA\Property( property="time_create", type="datetime", description="Дата-время создания" ),
	*							@OA\Property( property="href", type="string", description="Ссылка" )
	*						)
	*					)
	*				)
	*			)
	*		),
	*		@OA\Response(
	*			response=404,
	*			description="Не найдено",
	*		)
	*	)
	*/
	/**
	* Список блогов
	*
	* @param Request $request - запрос
	* @method GET
	*
	* @return string|array
	*/
	public function blogs( Request $request ) {
		$limit_value = $request->get( 'limit' ) ;

		if ( ! in_array( $limit_value , static::LIMIT_VALUES ) ) {
			$limit_value = current( static::LIMIT_VALUES ) ;
		}

		$time_create = $request->get( 'time_create' ) ;
		$time_create_value = null ;

		if ( ! empty( $time_create ) ) {
			$time_create_value = BlogModel::toTime( $time_create ) ;
		}

		$blogs = BlogModel::select( 'id' , 'title' , 'views' , 'time_create' , 'href' )->orderBy( 'time_create' , 'DESC' ) ;

		foreach ( array_filter( [ 'views' => $request->get( 'views' ) , 'time_create' => &$time_create_value , ] ) as $field => $value ) {
			$blogs->where( $field , $value ) ;
		}

		$product = $request->get( 'product' ) ;

		if ( ! empty( $product ) ) {
			$blogs->whereRaw( 'MATCH( `product` ) AGAINST( ? IN NATURAL LANGUAGE MODE WITH QUERY EXPANSION )' , [ $product , ] ) ;
		}

		$blogs = $blogs->paginate( $limit_value ) ;

		foreach ( $blogs as &$blog ) {
			$blog->time_create = BlogModel::fromTime( $blog->time_create ) ;
		}

		return $blogs->toJson( ) ;
	}

	/**
	*	@OA\Get(
	*		path="/api/v1/blog/{href}" ,
	*		operationId="blog" ,
	*		tags={"blog"},
	*		description="Экземпляр блога",
	*		@OA\Parameter( name="href" , description="Ссылка на Blog" , in="path" , @OA\Schema( type="string" ) ),
	*		@OA\Response(
	*			response=200,
	*			description="Успешная операция" ,
	*			@OA\JsonContent(
	*				@OA\Property(property="product", type="array", description="Информация о продукте",
	*					@OA\Items(
	*						type="array",
	*						collectionFormat="multi",
	*						@OA\Items(
	*							@OA\Property(property="name", type="string", description="имя"),
	*							@OA\Property(property="title", type="string", description="Заголовок")
	*						)
	*					)
	*				),
	*				@OA\Property(property="blog", type="array", description="Информация о блоге",
	*					@OA\Items(
	*						type="array",
	*						collectionFormat="multi",
	*						@OA\Items(
	*							@OA\Property(property="id", type="integer", description="идентификатор"),
	*							@OA\Property(property="title", type="string", description="Заголовок"),
	*							@OA\Property(property="href", type="string", description="Ссылка"),
	*							@OA\Property(property="body", type="string", description="Тело"),
	*							@OA\Property(property="description", type="string", description="Описание"),
	*							@OA\Property(property="product", type="string", description="Продукт"),
	*							@OA\Property(property="views", type="integer", description="Просмотры"),
	*							@OA\Property(property="time_create", type="integer", description="Дата-время создания")
	*						)
	*					)
	*				)
	*			)
	*		),
	*		@OA\Response(
	*			response=404,
	*			description="Не найдено",
	*		)
	*	)
	*/
	/**
	* Информация о блоге
	*
	* @param string $href - ссылка на блог
	* @method GET
	*
	* @return array
	*/
	public function blog( string $href ) {
		$blog = BlogModel::where( 'href' , $href )->first( ) ;

		if ( empty( $blog ) ) {
			abort( 404 ) ;
		}

		$blog->views ++ ;
		$blog->save( ) ;

		return [
			'blog' => [ $blog , ] ,
			'product' => ProductModel::where( 'name' , $blog->product )->get( ) ,
		] ;
	}
}