<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request as Request ;
use App\Models\Blog as BlogModel ;
use App\Models\Product as ProductModel ;
use App\Http\Controllers\Api as ApiController ;

/**
* Веб-представления данных
*/
class Web extends BaseController {
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

	/**
	* Список блогов
	*
	* @param Request $request - запрос
	*/
	public function blogs( Request $request ) {
		list( $views_min , $views_max ) = array_values(
			BlogModel::selectRaw( 'min( `views` ) AS `min` , max( `views` ) AS `max`' )->first( )->toArray( )
		) ;

		return view( 'blogs' , [
			'views_min' => &$views_min ,
			'views_max' => &$views_max ,
			'limit_values' => ApiController::LIMIT_VALUES ,
			'title' => __FUNCTION__ ,
		] ) ;
	}

	/**
	* Страница блога
	*
	* @param string $href - ссылка
	*/
	public function blog( string $href ) {
		$blog = BlogModel::select( 'title' )->where( 'href' , $href )->first( ) ;

		if ( empty( $blog ) ) {
			abort( 404 ) ;
		}

		return view( 'blog' , [
			'title' => $blog->title ,
			'href' => &$href ,
		] ) ;
	}
}