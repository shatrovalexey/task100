*** ЗАДАЧА
* в файле "Тестовое задание.docx"

*** НАСТРОЙКА
* настройте файл ".env"
* `composer install`

*** ЗАПУСК
* `php artisan migrate`
* `php artisan db:seed --class='Product'`
* `php artisan db:seed --class='Blog'`
* `php artisan serve`
* далее открыть в любом браузере `http://127.0.0.1:8000/`

*** АВТОР
Шатров Алексей Сергеевич <mail@ashatrov.ru>