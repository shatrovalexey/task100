<!DOCTYPE html>

<html>
	<head>
		<title>{{ $title }}</title>

		<meta charset="utf-8">
		<link rel="stylesheet" href="/css/style.css" type="text/css">
		<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
	</head>
	<body>
		<div class="layout">
			<div class="body">
				@yield('body')
			</div>
		</div>
		<script src="/js/script.js"></script>
	</body>
</html>