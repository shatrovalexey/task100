@extends('layout')
@section('body')
<sidebar class="sidebar">
	<form action="/api/v1/blogs" method="get" data-target="blogs">
		<input type="hidden" name="page">
		<fieldset>
			<legend>
				<span>фильтр</span>
			</legend>
			<label>
				<span>к-во просмотров</span>
				<input type="number" min="{{ $views_min }}" min="{{ $views_max }}" name="views">
			</label>
			<div class="both"></div>
			<label>
				<span>продукт</span>
				<input name="product">
			</label>
			<div class="both"></div>
			<label>
				<span>дата создания (даты обновления не определено в ТЗ)</span>
				<input type="datetime-local" name="time_create">
			</label>
			<div class="both"></div>
			<label>
				<span>к-во статей</span>
				<select name="limit">
					@foreach( $limit_values as $limit_value )
					<option value="{{ $limit_value }}">{{ $limit_value }}</option>
					@endforeach
				</select>
			</label>
			<div class="both"></div>
			<label>
				<span>найти</span>
				<input type="submit" value="&rarr;">
			</label>
		</fieldset>
	</form>
</sidebar>
<div class="content">
	<table border="1">
		<caption>
			<h2>список</h2>
		</caption>
		<thead>
			<th>
				<span>название</span>
			</th>
			<th width="70">
				<span>просм.</span>
			</th>
			<th width="150">
				<span>создан</span>
			</th>
		</thead>
		<tbody data-empty="нет данных">
			<tr data-template="blogs" data-ref="data">
				<td>
					<a href="/web/blog/@href" data-content="@title" target="@href"/>
				</td>
				<td align="right">
					<tt data-content="@views"/>
				</td>
				<td align="right">
					<tt data-content="@time_create"/>
				</td>
			</tr>
		</tbody>
		<tfoot>
			<tr>
				<td colspan="3">
					<ul class="pagination">
						<li data-template="blogs" data-ref="links">
							<a href="@url" data-page="@label" data-content="@label" data-active="@active" data-target="blogs"/>
						</li>
					</ul>
				</td>
			</tr>
		</tfoot>
	</table>
</div>
@stop