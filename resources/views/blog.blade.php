@extends('layout')
@section('body')
<form action="/api/v1/blog/{{$href}}" method="get" data-target="blog">
	<div data-template="blog" data-ref="blog" data-empty="Нет данных">
		<h1 title="блог" data-content="@title"></h1>
		<div data-content="@description"></div>
		<div data-content="@body"></div>
		<div data-content="@product"></div>
	</div>
</form>
@stop