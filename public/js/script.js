async function sha1( str ) {
    const buf = Uint8Array.from( unescape( encodeURIComponent( str ) ) , c => c.charCodeAt( 0 ) ).buffer ;
    const digest = await crypto.subtle.digest( 'SHA-1' , buf ) ;
    const raw = String.fromCharCode.apply( null , new Uint8Array( digest ) ) ;

    return btoa( raw ) ;
}

jQuery( function( ) {
	let $dotGet = function( $result , $ref ) {
		if ( $ref === null ) {
			return $result ;
		}

		for ( let $ref_current of $ref.split( '.' ) ) {
			if ( ! ( $ref_current in $result ) ) {
				return null ;
			}

			$result = $result[ $ref_current ] ;
		}

		return $result ;
	} ;
	let $parseTemplate = function( $data ) {
		return jQuery( "[data-template='" + this.attr( "data-target" ) + "']" ).each( function( ) {
			let $template = jQuery( this ) ;

			$template.nextAll( ).remove( ) ;

			let $result = $dotGet( $data , $template.data( "ref" ) ) ;
			let $empty_result = ! $result || ( Array.isArray( $result ) && ! $result.length ) ;

			$template.parent( )[ $empty_result ? "addClass" : "removeClass" ]( "template-values-empty" ) ;

			if ( $empty_result ) {
				return ;
			}

			jQuery( $result ).each( function( ) {
				let $data = this ;
				let $self = $template.clone( true ) ;

				$template.after( $self ) ;

				$self.find( "*" ).each( function( ) {
					let $elem = this ;

					jQuery.each( $elem.attributes , function( ) {
						this.value = this.value.replace( /\@(\w+)/g , function( $matches ) {
							let $key = RegExp.$1 ;

							if ( ! ( $key in $data ) ) {
								return $matches ;
							}

							return $data[ $key ] ;
						} ) ;

						switch ( this.name ) {
							case "data-content" : {
								$elem.innerHTML = this.value ;
							}
						}
					} ) ;
				} ) ;
			} ) ;
		} ) ;
	} ;

	let $queryResult = function( $url , $data , $method ) {
		jQuery.ajax( {
			"url" : $url ,
			"type" : $method ,
			"dataType" : "json" ,
			"data" : $data ,
			"context" : this ,
			"success" : function( $response ) {
				if ( ! $parseTemplate.call( this , $response ) ) {
					return ;
				}

				sha1( [ $url , $data , $method ].join( '\n' ) ).then( function( $hash ) {
					location.hash = $hash ;
				} ) ;
			}
		} ) ;

		return false 
	} ;

	let $form = jQuery( "form[data-target]" ).on( "submit" , function( ) {
		let $self = jQuery( this ) ;

		return $queryResult.call( $self , $self.attr( "action" ) , $self.serialize( ) , $self.attr( "method" ) ) ;
	} ) ;
	let $page_input = $form.find( "input[name=page]" ) ;

	$form.find( "input, select" ).on( "change" , function( ) {
		$page_input.removeAttr( "value" ) ;
	} ) ;
	jQuery( ".pagination > li > a" ).on( "click" , function( ) {
		let $self = jQuery( this ) ;

		$page_input.val( $self.data( "page" ) ) ;
		$form.submit( ) ;

		return false ;

		// return $queryResult.call( $self , $self.attr( "href" ) , null , "GET" ) ;
	} ) ;

	$form.submit( ) ;
} ) ;