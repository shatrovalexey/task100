<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Product as ProductModel ;

class ProductCreate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up( ) {
		DB::statement( "
CREATE TABLE `" . app( ProductModel::class )->getTable( ) . "`(
	`name` VARCHAR( 255 ) NOT null COMMENT 'название' ,
	`href` VARCHAR( 255 ) NOT null COMMENT 'ссылка' ,

	PRIMARY KEY( `name` ) ,
	INDEX( `href` )
) COMMENT 'продукт' ;
		" ) ;
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down( ) {
		DB::statement( "
DROP TABLE IF EXISTS `" . app( ProductModel::class )->getTable( ) . "` ;
		" ) ;
    }
}
