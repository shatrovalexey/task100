<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Blog as BlogModel ;
use App\Models\Product as ProductModel ;

class BlogCreate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up( ) {
		DB::statement( "
CREATE TABLE `" . app( BlogModel::class )->getTable( ) . "`(
	`id` BIGINT UNSIGNED NOT null AUTO_INCREMENT COMMENT 'идентификатор' ,
	`href` VARCHAR( 100 ) NOT null COMMENT 'ссылка' ,
	`title` VARCHAR( 255 ) NOT null COMMENT 'заголовок' ,
	`body` TEXT NOT null COMMENT 'тело' ,
	`description` TEXT NOT null COMMENT 'описание' ,
	`product` VARCHAR( 255 ) NOT null COMMENT 'продукт' ,
	`views` INT UNSIGNED NOT null COMMENT 'просмотры' ,
	`time_create` INT UNSIGNED NOT null COMMENT 'дата-время создания' ,

	PRIMARY KEY( `id` ) ,
	INDEX( `time_create` , `views` ) ,
	INDEX( `href` ) ,
	FULLTEXT( `product` ) ,
	FOREIGN KEY ( `product` ) REFERENCES `" . app( ProductModel::class )->getTable( ) . "`( `name` ) ON UPDATE CASCADE ON DELETE CASCADE
) COMMENT 'блог' ;
		" ) ;
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down( ) {
		DB::statement( "
DROP TABLE IF EXISTS `" . app( BlogModel::class )->getTable( ) . "` ;
		" ) ;
    }
}
