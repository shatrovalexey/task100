<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Product as ProductModel ;
use DB ;

class Product extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run( ) {
		/**
		* Понимаю, можно было бы использовать метод модели для создания записи. Но это дольше, чем работать напрямую.
		* Понимаю, можно было использовать загрузку в БД из файла CSV, это было бы крайне быстро. Но тогда нужно было бы ещё преобразование JSON к CSV и дополнительные права пользователя для использования ФС.
		*/
		$data = @json_decode( file_get_contents( __DIR__ . '/data/' . app( ProductModel::class )->getTable( ) . '.json' ) , true ) ;

		if ( empty( $data[ 'data' ] ) || ! is_array( $data[ 'data' ] ) ) {
			return ;
		}

		$dbh = DB::connection()->getPdo( ) ;
		$sth = $dbh->prepare( "
INSERT IGNORE INTO
	`" . app( ProductModel::class )->getTable( ) . "`
SET
	`name` := :name ,
	`href` := :href ;
		" ) ;

		foreach ( $data[ 'data' ] as &$row ) {
			$sth->execute( [
				':name' => &$row[ 'name' ] ,
				':href' => &$row[ 'href' ] ,
			] ) ;
		}

		$sth->closeCursor( ) ;
    }
}
