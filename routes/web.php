<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api as ApiController ;
use App\Http\Controllers\Web as WebController ;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group( [
	'prefix' => 'api/v1' ,
] , function( $route ) {
	$route->get( 'blogs' , [ ApiController::class , 'blogs' , ] ) ;
	$route->get( 'blog/{href}' , [ ApiController::class , 'blog' , ] ) ;
	$route->post( 'blog/{id}' , [ ApiController::class , 'blog_update' , ] ) ;
} ) ;

Route::group( [
	'prefix' => 'web' ,
] , function( $route ) {
	$route->get( 'blogs' , [ WebController::class , 'blogs' , ] ) ;
	$route->get( 'blog/{href}' , [ WebController::class , 'blog' , ] ) ;
} ) ;
Route::get( '/' , [ WebController::class , 'blogs' , ] ) ;